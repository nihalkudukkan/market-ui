import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import SideNav from './SideNav';

export default function BusinessHome() {
    const [items, setItems] = useState([]);

    const history = useHistory();
    useEffect(()=>{
        let token = sessionStorage.getItem("ownerToken");
        if (!token) {
            history.push("/businessportal")
        }
        axios.get('https://floating-tor-03177.herokuapp.com/owner/get', {headers: {Authorization:token}})
            .then(res=>setItems(res.data))
    },[history])

    return (
        <>
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-2 d-flex flex-column portal_nav">
                    <SideNav page={"home"}/>
                </div>
                <div className="col-md-10 item_page item_bus">
                    <div className="container-lg">
                    <div className="row">
                        {items && items.map((item, k)=>(
                            <ItemBox item={item} key={k} />
                        ))}
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

function ItemBox({item, k}) {
    const history = useHistory();
    const toEdit = () => {
        history.push(`/businessportal/edititem?itemId=${item._id}`)
    }
    return (
        <div className="d-flex col-xl-3 col-lg-4 col-sm-6 p-3" key={k}>
            <div className="card card-bus p-3">
                <div className="">
                    <div className="item_img">
                        <img src={item.image} alt="" />
                    </div>
                    <div className="item_name">
                        {item.name}
                    </div>
                    <div className="">
                        {item.info}
                    </div>
                    <div className="item_price">
                        ₹ {item.price}
                    </div>
                    <div className="">
                        Available-{item.count}
                    </div>
                    <div className="">
                        Total Sales-0
                    </div>
                </div>
                <div className="d-flex justify-content-evenly">
                    <button className="btn btn-primary" onClick={toEdit}>Edit</button>
                    <button className="btn btn-success">Add</button>
                </div>
            </div> 
        </div>
    )
}