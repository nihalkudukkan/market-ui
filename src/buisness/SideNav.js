import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'

export default function SideNav({page}) {
    const history = useHistory();
    const [homeClass, setHClass] = useState("portal_btn")
    const [addClass, setAClass] = useState("portal_btn")

    useEffect(()=>{
        if (page==="home") {
            setHClass("portal_btn active")
        }
        if (page==="add") {
            setAClass("portal_btn active")
        }
    },[page])

    const logout = () => {
        sessionStorage.clear();
        history.push('/businessportal')
    }
    return (
        <>
        <div className="market_head">
            <h2>Market Portal</h2>
        </div>
        <Link to="/businessportal/items" className={homeClass}>Home</Link>
        <Link to="/businessportal/additem" className={addClass}>Add Item</Link>
        <button className="portal_btn" onClick={logout}>Log Out</button>  
        </>
    )
}
