import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useLocation, useHistory } from 'react-router-dom';
import SideNav from './SideNav';

export default function BusinessEdit() {
    let query = useQuery();
    const history = useHistory();
    const [token, setToken] = useState("")
    let itemId = query.get("itemId")
    let [item, setItem] = useState({});

    useEffect(()=>{
        let token = sessionStorage.getItem("ownerToken");
        if (!token) {
            history.push("/businessportal")
        }
        setToken(token)
    },[history])

    useEffect(()=>{
        if (itemId) {
            setLoadingC(true)
            const fetchData = async() => {
                let response = await axios.get(`https://floating-tor-03177.herokuapp.com/consumer/item?id=${itemId}`)
                setItem(response.data)
                setName(response.data.name)
                setInfo(response.data.info)
                setPrice(response.data.price)
                setCount(response.data.count)
                setLoadingC(false)
            }
            fetchData()
        }
    },[itemId])
    // const [token, setToken] = useState("");
    const [name, setName] = useState("")
    const [info, setInfo] = useState("")
    const [price, setPrice] = useState("")
    const [count, setCount] = useState("")
    const [image, setPic] = useState("")
    const [loading, setLoading] = useState(false)
    const [loadingC, setLoadingC] = useState(false)

    const postNewData = async(e) => {
        try {
            setLoading(true)
            e.preventDefault()
        let pic;

        if (!image) {
            pic = item.image
        } else {
            const data = new FormData();
            
            data.append("file",image)
            data.append("upload_preset","market-pic")
            data.append("cloud_name","nihaltheblade")

            let imageRes = await axios.post('https://api.cloudinary.com/v1_1/nihaltheblade/image/upload',data)
            pic = imageRes.data.url;
        }
        
        await axios.put('https://floating-tor-03177.herokuapp.com/owner/update', {id: item._id, name, info, price, count, image: pic}, {headers: {Authorization:token}})
        history.push('/businessportal/items')
        } catch (error) {
            console.log({error: error.response});
            setLoading(false)
        }
    }

    return (
        <>
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-2 d-flex flex-column portal_nav">
                    <SideNav/>
                </div>
                <div className="col-md-10 Add_item p-3">
                {loadingC?<div className="spinner-border text-primary mt-3" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>:
                        <div className="card px-5 py-4">
                            <div className="head text-center">
                                Edit Item
                            </div>
                            <form onSubmit={postNewData}>
                                <p>Item Name</p>
                                <input type="text" value={name} onChange={(e)=>setName(e.target.value)} required/>
                                <p>Item Info</p>
                                <input type="text" value={info} onChange={(e)=>setInfo(e.target.value)} required/>
                                <p>Price</p>
                                <input type="number" value={price} onChange={(e)=>setPrice(e.target.value)} min="0" required/>
                                <p>Count</p>
                                <input type="number" value={count} onChange={(e)=>setCount(e.target.value)} min="0" required/>
                                <p>Current Pic</p>
                                <img src={item.image} alt="" />
                                <p>pic</p>
                                <input type="file" accept="image/*" onChange={(e)=>setPic(e.target.files[0])}/>
                                {loading?<div className="spinner-border text-primary mt-3" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>:
                                        <input className="btn btn-primary mt-3" type="submit" value="Save" />
                                }
                            </form>
                        </div>
                }
                </div>
            </div>
        </div>
        </>
    )
}

function useQuery() {
    return new URLSearchParams(useLocation().search);
  }