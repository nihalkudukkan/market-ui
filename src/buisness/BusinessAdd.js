import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import SideNav from './SideNav';

export default function BusinessAdd() {
    const [token, setToken] = useState("");
    const [name, setName] = useState("")
    const [info, setInfo] = useState("")
    const [price, setPrice] = useState("")
    const [count, setCount] = useState("")
    const [pic, setPic] = useState("")
    const [loading, setLoading] = useState(false)
    const history = useHistory();
    useEffect(()=>{
        let tok = sessionStorage.getItem("ownerToken");
        if (!tok) {
            history.push("/businessportal")
        }
        setToken(tok);
    },[history, token])

    const postData = async(e) => {
        try {
            e.preventDefault();
            setLoading(true);
            const data = new FormData();
            
            data.append("file",pic)
            data.append("upload_preset","market-pic")
            data.append("cloud_name","nihaltheblade")

            let imageRes = await axios.post('https://api.cloudinary.com/v1_1/nihaltheblade/image/upload',data)
            let image = imageRes.data.url;
            await axios.post('https://floating-tor-03177.herokuapp.com/owner/add', {name: name.trim().toLocaleUpperCase(), price, count, image, info}, {headers: {Authorization:token}})
            
            alert("added successfully");
            history.push('/businessportal/items')
        } catch (error) {
            console.log(error.response);
        }
    }
    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-2 d-flex flex-column portal_nav">
                        <SideNav page={"add"}/>
                    </div>
                    <div className="col-md-10 Add_item">
                            <div className="card px-5 py-4">
                                <div className="head text-center">
                                    Add Item
                                </div>
                                <form onSubmit={postData}>
                                    <p>Item Name</p>
                                    <input type="text" value={name} onChange={(e)=>setName(e.target.value)} required/>
                                    <p>Item Info</p>
                                    <input type="text" value={info} onChange={(e)=>setInfo(e.target.value)} required/>
                                    <p>Price</p>
                                    <input type="number" value={price} onChange={(e)=>setPrice(e.target.value)} min="0" required/>
                                    <p>Count</p>
                                    <input type="number" value={count} onChange={(e)=>setCount(e.target.value)} min="0" required/>
                                    <p>pic</p>
                                    <input type="file" accept="image/*" onChange={(e)=>setPic(e.target.files[0])} required/>
                                    {loading?<div className="spinner-border text-primary mt-3" role="status">
                                                <span className="visually-hidden">Loading...</span>
                                            </div>:
                                            <input className="btn btn-primary mt-3" type="submit" value="Add" />
                                            }
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </>
    )
}
