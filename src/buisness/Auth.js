import axios from 'axios';
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';

export default function Auth() {
    const [owner, setOwner] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState("")
    const [error, setError] = useState("")
    
    const history = useHistory();

    const postData = async(e) => {
        try {
            e.preventDefault();
            setLoading("loading")

            let message = await axios.post("https://floating-tor-03177.herokuapp.com/owner/signin",{owner, password})
            setLoading("");
            sessionStorage.setItem("ownerToken", message.data.token)
            sessionStorage.setItem("ownerUser", JSON.stringify(message.data.user))
            history.push('/businessportal/items')
        } catch (error) {
            setError(error.response.data);
            setLoading("");
        }
    }

    return (
        <>
        <div className="business">
            <div className="sign__up">
                <div className="card px-5 py-4 business_auth">
                    <div className="head text-center">
                        Sign Up For Business
                    </div>
                    <form onSubmit={postData}>
                        <p>Auth Id</p>
                        <input type="text" value={owner} onChange={(e)=>setOwner(e.target.value)} required/>
                        <p>Password</p>
                        <input type="password" value={password} onChange={(e)=>setPassword(e.target.value)} required/>
                        {loading?<div className="spinner-border text-primary mt-3" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>:
                        <input className="btn btn-primary mt-3" type="submit" value="Sign In"/>
                    }
                    {error && <div className="alert alert-danger mt-3" role="alert">
                        {error.error}
                    </div>}
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}
