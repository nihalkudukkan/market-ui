let initialState = {
    user: {},
    token: ""
}

export const userReducer = (state=initialState, action) => {

    switch (action.type) {
        case 'Login':
            return {user: action.user, token: action.token};
        case 'Logout':
            return {user: {}, token: ""};
        default:
            return state;
    }
}