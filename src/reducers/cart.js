let initialState = []

export const cartReducer = (state=initialState, action) => {
    switch (action.type) {
        case 'ADD':
            return action.cart
        case 'OUT':
            return []
        default:
            return state
    }
}