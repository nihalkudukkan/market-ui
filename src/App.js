import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Products from './customer/Products';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Item from './customer/Item';
import SignUp from './customer/SignUp';
import LogIn from './customer/LogIn';
import Auth from './buisness/Auth';
import BusinessHome from './buisness/BusinessHome';
import BusinessAdd from './buisness/BusinessAdd';
import BusinessEdit from './buisness/BusinessEdit';
import Cart from './customer/Cart';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/businessportal" component={Auth} exact/>
          <Route path="/businessportal/items" component={BusinessHome} exact/>
          <Route path="/businessportal/additem" component={BusinessAdd} exact/>
          <Route path="/businessportal/edititem" component={BusinessEdit} exact/>
          <Route path="/login" component={LogIn} exact/>
          <Route path="/signup" component={SignUp} exact/>
          <Route path="/item/:id" component={Item} exact/>
          <Route path="/cart" component={Cart} exact/>
          <Route path="/" component={Products} exact/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
