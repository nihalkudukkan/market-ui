export const login = (user, token) => {
    return {
        type:'Login',
        user,
        token
    }
}

export const logout = () => {
    return {
        type:'Logout'
    }
}

// cart actions

export const addtoCart = (x) => {
    return {
        type: 'ADD',
        cart: x
    }
}

export const outOfCart = () => {
    return {
        type: 'OUT'
    }
}