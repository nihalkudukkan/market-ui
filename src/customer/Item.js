import React, { useEffect, useState } from 'react'
import Navbar from './Navbar'
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import NoProd from './NoProd';
import { addtoCart } from '../actions';

export default function Item() {
    let { id } = useParams();
    const [item, setItem] = useState({})
    // const [cart, setCart] = useState("")
    const [incart, setinCart] = useState(false)

    const [loadingC, setLoadingC] = useState(false)
    const [loadingB, setLoadingB] = useState(false)
    const [broken, setBrok] = useState(false)

    const user = useSelector(state=>state.Auth)
    const history = useHistory()
    const dispatch = useDispatch();

    useEffect(()=>{
        setLoadingC(true)
        axios.get(`https://floating-tor-03177.herokuapp.com/consumer/item?id=${id}`)
            .then(res=>{
                setItem(res.data);
                // axios.get('https://floating-tor-03177.herokuapp.com/user/cart/get', {headers:{Authorization: user.token}})
                //     .then(res=>{setCart(res.data.items);})
                axios.get(`https://floating-tor-03177.herokuapp.com/user/cart/check?id=${id}`, {headers:{Authorization: user.token}})
                    .then(item=>{
                        if (!item.data) {
                            setLoadingC(false)
                        }
                        else if (item) {
                            setinCart(true);setLoadingC(false)
                        }
                    })
                    .catch(err=>{console.log("user does not has cart");setLoadingC(false)})
            })
            .catch(err=>{setLoadingC(false);setBrok(true);console.log("No data");})
    },[id, user])

    // useEffect(()=>{
    //     if (cart) {
    //         setLoadingC(true);
    //         cart.forEach(i=>{
    //             if (i.id===id) {
    //                 setinCart(true);
    //             }
    //         })
    //         setLoadingC(false)
    //     }
    // },[cart, id])

    const addToCart = () => {
        setLoadingB(true)
        if (!user.token) {
            return history.push(`/login?path=${history.location.pathname}`)
        }
        setinCart(true)
        axios.put('https://floating-tor-03177.herokuapp.com/user/cart/add',{count:1, itemId:id}, {headers:{Authorization: user.token}})
            .then(res=>{
                console.log("added");
                axios.get('https://floating-tor-03177.herokuapp.com/user/cart/get', {headers:{Authorization: user.token}})
                    .then(cart=>{
                        console.log(cart.data);
                        if (cart.data) {
                            // console.log("add");
                            dispatch(addtoCart(cart.data.items));
                            setLoadingB(false)
                        }
                    })
            })
            .catch(err=>{console.log(err);})
        console.log("added to cart");
    }
    if (!broken) {
        return (
            <>
            <Navbar page={"item"} />
            <div className="item_page m-3">
            {loadingC?<div className="spinner-border text-primary mt-3" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>:
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="item_img p-4">
                            <img src={item.image} alt="" />
                        </div>
                    </div>
                    <div className="col-md-6 p-4">
                        <div className="item_name">
                            {item.name}
                        </div>
                        <hr />
                        <div className="item_price">
                            $ {item.price}
                        </div>
                        <hr />
                        <div className="d-flex">
                            <button className="btn btn-primary m-3">Buy now</button>
                            {loadingB?
                            <div className="d-flex align-items-center mx-3">
                                    <div className="spinner-border text-warning" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                            </div>
                            :
                                <>
                                {incart?<button className="btn btn-warning m-3" onClick={()=>history.push('/cart')}>Go to cart</button>:
                                <button className="btn btn-warning m-3" onClick={addToCart}>Add to cart</button>
                                }
                                </>
                            }
                        </div>
                    </div>
                </div>
            </div>
            }
            </div>
            </>
        )
    }
    return (
        <>
            <Navbar />
            <NoProd />
        </>
    )
}