import axios from 'axios'
import React, { useEffect, useRef, useState } from 'react'
import { Link } from 'react-router-dom'
import Navbar from './Navbar'

export default function SignUp() {
    const [fname, setF] = useState("")
    const [lname, setL] = useState("")
    const [phone, setPhone] = useState("")
    const [email, setEm] = useState("")
    const [password, setPass] = useState("")
    const [error, setError] = useState("")
    const [created, setCreate] = useState(false)
    const [loading, setLoad] = useState(false)

    const first = useRef(null);

    useEffect(()=>{
        first.current.focus();
    },[])

    const postData = (e) => {
        e.preventDefault();
        let data = {
            fname: fname.trim().toLowerCase(), lname: lname.trim().toLowerCase(), phone: phone.trim(), email, password
        }
        setLoad(true)
        axios.post('https://floating-tor-03177.herokuapp.com/user/signup', data)
            .then(res=>{
                setCreate(true)
                setLoad(false)
            })
            .catch(err=>{setError(err.response.data);setLoad(false)})
    }

    return (
        <>
        <Navbar page={"signup"}/>
        <div className="sign__up mt-3">
            <div className="card px-5 py-4">
                {created? <FinishBox />:
                <>
                <div className="head text-center">
                Sign Up
            </div>
            <form onSubmit={postData}>
                <p>First Name</p>
                <input ref={first} type="text" value={fname} onChange={(e)=>setF(e.target.value)}  required/>
                <p>Last Name</p>
                <input type="text" value={lname} onChange={(e)=>setL(e.target.value)} required/>
                <p>Phone</p>
                <input type="tel" value={phone} onChange={(e)=>setPhone(e.target.value)} required/>
                <p>Email</p>
                <input type="email" value={email} onChange={(e)=>setEm(e.target.value)} required/>
                <p>Password</p>
                <input type="password" value={password} onChange={(e)=>setPass(e.target.value)} required/>
                {loading?<div className="spinner-border text-primary mt-3" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>:
                        <input className="btn btn-primary mt-3" type="submit" value="Sign Up"/>
                        }
                {/* <div className="text-danger mt-1">{error && error.error}</div> */}
                {error && <div className="alert alert-danger mt-3" role="alert">
                    {error.error}
                </div>}
            </form>
            </>
                }
            </div>
        </div>
        </>
    )
}

const FinishBox = () => {
    return(
        <>
            <h3>Finish signing up</h3>
            <p>By signing up, you agree to our terms and conditions. Go to login page to login to you account</p>
            <Link to="/login" className="btn btn-primary">Login</Link>
        </>
    )
}