import ShoppingBasketOutlinedIcon from '@material-ui/icons/ShoppingBasketOutlined';
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { addtoCart, logout, outOfCart } from '../actions';
import { useEffect, useState } from 'react';
import service from './Services/service';
import axios from 'axios';

export default function Navbar({page}) {
    const history = useHistory();
    const [navLink, setNavlink] = useState(
        {
            signup: "col-lg-1 col-md-2 col-sm-6 nav_link",
            login: "col-lg-1 col-md-2 col-sm-6 nav_link"
        }
        );
    const user = useSelector(state=>state.Auth)

    useEffect(()=>{        
        setNavlink(service.navBarNotAuth(page))
    },[page])

    return (
            <div className="navbar__1">
                <div className="container-fluid">
                    <div className="row">
                        {user.token? <><Logged user={user.user} token={user.token} page={page}/></>:
                        <>
                        <div className="col-lg-1 nav_space">
                        </div>
                        <div className="col-lg-8 col-md-7 col-sm-12 d-flex justify-content-center align-items-center">
                            <div className="brand" onClick={()=>history.push('/')}>MARKET</div>
                        </div>
                        <div className={navLink.signup}>
                            <div style={{cursor: 'pointer'}} onClick={()=>history.push('/signup')}>Sign Up</div>
                        </div>
                        <div className={navLink.login}>
                            <div style={{cursor: 'pointer'}} onClick={()=>history.push('/login')}>Log In</div>
                        </div>
                        </>
                        }
                    </div>
                </div>
            </div>
    )
}

function Logged({user, page, token}) {
    const history = useHistory();
    const dispatch = useDispatch();

    const [name, setName] = useState("")
    const cart = useSelector(state=>state.Cart)
    const [navLink, setNavLink] = useState({
        default: "col-lg-1 col-md-2 col-sm-4 nav_link",
        cart: "col-lg-1 col-md-2 col-sm-4 nav_link"
    })
    // cart load
    useEffect(()=>{
        axios.get('https://floating-tor-03177.herokuapp.com/user/cart/get', {headers:{Authorization: token}})
            .then(cart=>{
                if (cart.data) {
                    // console.log("add");
                    dispatch(addtoCart(cart.data.items));
                }
            })
    },[token, history, dispatch])

    useEffect(()=>{
        if (user) {
            let name = service.toFirstUpperCase(user)
            setName(name)
        }
    },[user])

    useEffect(()=>{
        if (page) {
            setNavLink(service.navBarAuth(page))
        }
    },[page])

    const logOut = () => {
        dispatch(outOfCart())
        dispatch(logout());
    }
    return(
        <>
            <div className="col-lg-1 nav_space">

            </div>
            <div className="col-lg-8 col-md-5 col-sm-12 d-flex justify-content-center align-items-center">
                <div className="brand" onClick={()=>history.push('/')}>MARKET</div>
            </div>
            <div className={navLink.default}>
                <div style={{cursor: 'pointer'}}>{name}</div>
            </div>
            <div className={navLink.default}>
            <div style={{cursor: 'pointer'}} onClick={logOut}>Log Out</div>
            </div>
            <div className={navLink.cart}>
            <ShoppingBasketOutlinedIcon className="clickable" onClick={()=>history.push('/cart')}/>
            <span className="position-absolute top-0 translate-middle badge rounded-pill bg-danger">
                {cart.length}
                <span className="visually-hidden">unread messages</span>
            </span>
            </div>
        </>
    )
}