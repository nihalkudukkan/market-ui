import React from 'react'
import HelpIcon from '@mui/icons-material/Help';
import {Link} from 'react-router-dom'

export default function NoProd() {
    return (
        <div className="link_error">
            <div className="error_box">
                <HelpIcon fontSize="large"/>
                <p className="error_cause">Invalid link address</p>
                <p className="error_redirect">Go to Market App's Home page</p>
                <Link to="/">Market.in</Link>
            </div>
        </div>
    )
}
