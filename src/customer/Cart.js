import React, { useEffect, useState } from 'react'
import Navbar from './Navbar'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';
import { addtoCart } from '../actions';
import AddShoppingCartOutlinedIcon from '@mui/icons-material/AddShoppingCartOutlined';

export default function Cart() {
    const user = useSelector(state=>state.Auth);
    const cart = useSelector(state=>state.Cart);
    const history = useHistory();

    useEffect(()=>{
        if (!user.token) {
            history.push(`/login?path=${history.location.pathname}`)
        }
    },[user, history])

    // useEffect(()=>{
    //     if (cart) {
    //      console.log(cart);   
    //     }
    // },[cart])

    return (
        <>
        <Navbar page={"cart"}/>
        <div className="cart py-3">
            <div className="container-sm">
                <div className="row">
                    <div className="col-md-8">
                        <div className="card card_items mb-3">
                            <div className="cart_head">
                                My Cart
                            </div>
                            <div className="cart_items">
                                {cart.length!==0?cart.map((i, k)=>(
                                    <CartItem key={k} cart={i} token={user.token}/>
                                )):
                                <div className="cart_empty">
                                    <AddShoppingCartOutlinedIcon className="cart_empty_icon"/>
                                    <p>Your cart is empty!</p>
                                    <Link to="/" className="btn btn-primary">Shop Now</Link>
                                </div>
                                }
                            </div>
                            <div className="cart_order">
                                <button className="btn btn-warning">Place Order</button>
                            </div>
                        </div>
                    </div>
                    <PriceDetails cart={cart}/>
                </div>
            </div>
        </div>  
        </>
    )
}


function CartItem({cart, token}) {
    const dispatch = useDispatch()
    const [item, setItem] = useState("")
    const [loadingB, setLoadingB] = useState(false)

    // loading cart item
    useEffect(()=>{
        axios.get(`https://floating-tor-03177.herokuapp.com/consumer/item?id=${cart.id}`)
            .then(item=>{
                setItem(item.data)
            })
    },[cart])

    const remove = (id) => {
        setLoadingB(true)
        axios.put('https://floating-tor-03177.herokuapp.com/user/cart/remove', {itemId: id}, {headers: {authorization: token}})
            .then(data=>{
                axios.get('https://floating-tor-03177.herokuapp.com/user/cart/get', {headers:{Authorization: token}})
                    .then(response=>{dispatch(addtoCart(response.data.items));setLoadingB(false)})
                    .catch(err=>{console.log(err.response);setLoadingB(false)})
            })
            .catch(err=>{console.log(err.response);setLoadingB(false)})
    }

    return(
        <div className="cart_item">
            <div className="cart_image_box">
                <div className="cart_item_image">
                    <img src={item.image} alt="" />
                    <p>Count</p>
                    <div className="count_box">
                        <input type="number" defaultValue="1" min="1" max="5"/>
                    </div>
                </div>
            </div>
            <div className="cart_details_box">
                <div className="">
                    <Link className="cart_item_name" to={`item/${item._id}`}>{item.name}</Link>
                    <div className="cart_item_info">{item.info}</div>
                    <div className="cart_item_seller">Seller: {item.owner}</div>
                    <div className="cart_item_price">₹ {item.price}</div>
                </div>
                <div className="cart_item_remove">
                {loadingB?
                    <div className="d-flex align-items-center mx-3">
                            <div className="spinner-border text-danger" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                    </div>
                    :
                    <button className="btn btn-danger" onClick={()=>remove(item._id)}>Remove</button>
                }
                </div>
            </div>
        </div>
    )
}

function PriceDetails({cart}) {
    const [total, setTotal] = useState(0);
    // cart item load and price calculation
    useEffect(()=>{
        let price = 0;
        if (cart.length===0) {
            setTotal(0)
        }
        cart.forEach(i=>{
            axios.get(`https://floating-tor-03177.herokuapp.com/consumer/item?id=${i.id}`)
                .then(response=>{
                    // console.log(response.data.price);
                    price = price + response.data.price;
                    setTotal(price)
                })
            })
        console.log(1);
    },[cart])
    return(
        <div className="col-md-4">
            <div className="card">
                <div className="cart_total_head">PRICE DETAILS</div>
                <div className="price_details">
                    <div className="cart_details_price">
                        <div className="">Price</div>
                        <div className="">₹ {total}</div>
                    </div>
                    <div className="cart_details_delivery">
                        <div className="">Delivery Charges</div>
                        <div className="">FREE</div>
                    </div>
                    <hr />
                </div>
                <div className="cart_total_amount">
                    <div className="">Total Amount</div>
                    <div className="">₹ {total}</div>
                </div>
            </div>
        </div>
    )
}