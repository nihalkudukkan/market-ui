import React, { useEffect, useState } from 'react'
import Navbar from './Navbar'
import axios from 'axios';
import { useHistory } from 'react-router';

export default function Products() {
    // const history = useHistory();
    const [items, setItems] = useState([]);

    useEffect(()=>{
        axios.get('https://floating-tor-03177.herokuapp.com/consumer/all')
            .then(res=>{setItems(res.data)})
            .catch(err=>console.log(err))
    },[])
    return (
        <>
        <Navbar page={"home"}/>
        <div className="product__page mt-3">
            <div className="container">
                <div className="row">
                    {items && items.map((item, k)=>(
                        <ItemBox item={item} key={k} />
                    ))}
                </div>
            </div>
        </div>
        </>
    )
}

function ItemBox({item, k}) {
    const history = useHistory();
    const itemPage = () =>{
        history.push(`/item/${item._id}`)
    }
    return (
        <div className="col-md-4 p-3" key={k}>
            <div className="card" onClick={itemPage}>
                <div className="clickable">
                    <div className="item_img">
                        <img src={item.image} alt="" />
                    </div>
                    <div className="item_details">
                        <div className="item_name">
                            {item.name}
                        </div>
                        <div className="">
                            {item.info}
                        </div>
                        <div className="item_price">
                            ₹ {item.price}
                        </div>
                    </div>
                    {/* <div className="">
                        Available-{item.count}
                    </div>
                    <div className="">
                        Total Sales-0
                    </div> */}
                </div>
                
                {/* <div className="cart_add">
                    <button className="btn btn-primary" onClick={()=>console.log("hi")}>Add to Cart</button>
                </div> */}
            </div> 
        </div>
    )
}