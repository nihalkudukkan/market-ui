import axios from 'axios'
import React, { useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory, useLocation } from 'react-router-dom'
import { login } from '../actions'
import Navbar from './Navbar'

export default function LogIn() {
    let query = useQuery();

    const history = useHistory();

    const [data, setData] = useState("")
    const [password, setPass] = useState("")
    const [loading, setLoad] = useState(false)
    const [error, setError] = useState("")

    const first = useRef(null);

    const dispatch = useDispatch();

    useEffect(()=>{
        first.current.focus();
    },[])

    const postData = (e) => {
        e.preventDefault();
        setLoad(true)
        let datas = {
            data: data.trim(), password
        }
        axios.post('https://floating-tor-03177.herokuapp.com/user/signin', datas)
            .then(res=>{
                setLoad(false);
                dispatch(login(res.data.user, res.data.token))
                if (query.get("path")) {
                    history.push(query.get("path"))
                } else {
                    history.push('/')
                }
            })
            .catch(err=>{console.log(err.response.data.error);setError(err.response.data);setLoad(false)})
    }
    return (
        <>
        <Navbar page={"login"}/>
        <div className="sign__up mt-3">
            <div className="card px-5 py-4">
                <div className="head text-center">
                    Login
                </div>
                <form onSubmit={postData}>
                    <p>Email or Phone</p>
                    <input ref={first} type="text" value={data} onChange={(e)=>setData(e.target.value)} required/>
                    <p>Password</p>
                    <input type="password" value={password} onChange={(e)=>setPass(e.target.value)} required/>
                    {loading?<div className="spinner-border text-primary mt-3" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>:
                        <input className="btn btn-primary mt-3" type="submit" value="Log In"/>
                    }
                    {error && <div className="alert alert-danger mt-3" role="alert">
                        {error.error}
                    </div>}
                </form>
            </div>
        </div>
        </>
    )
}

function useQuery() {
    return new URLSearchParams(useLocation().search);
}