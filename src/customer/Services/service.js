class Service {
    toFirstUpperCase(string) {
        let name = string.fname.charAt(0).toUpperCase() + string.fname.slice(1)
        return name
    }
    navBarNotAuth(page) {
        if(page==="signup") {
            return {
                signup: "col-lg-1 col-md-2 col-sm-6 nav_link nav_link_active",
                login: "col-lg-1 col-md-2 col-sm-6 nav_link"
            }
        } else if(page==="login") {
            return {
                signup: "col-lg-1 col-md-2 col-sm-6 nav_link",
                login: "col-lg-1 col-md-2 col-sm-6 nav_link nav_link_active"
            }
        } else {
            return {
                signup: "col-lg-1 col-md-2 col-sm-6 nav_link",
                login: "col-lg-1 col-md-2 col-sm-6 nav_link"
            }
        }
    }
    navBarAuth(page) {
        if(page==="cart") {
            return {
                default: "col-lg-1 col-md-2 col-sm-4 nav_link",
                cart: "col-lg-1 col-md-2 col-sm-4 nav_link nav_link_active"
            }
        } else {
            return {
                default: "col-lg-1 col-md-2 col-sm-4 nav_link",
                cart: "col-lg-1 col-md-2 col-sm-4 nav_link"
            }
        }
    }
}

let service = new Service()

module.exports = service;